# How to contribute

1. Fork the repository
2. Complete your work in a new git branch
3. Commit to your own repository's new branch
4. Submit a pull request back to this repository new branch name

