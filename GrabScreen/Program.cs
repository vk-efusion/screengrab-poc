﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading;
using BjSTools.Forms;
using BjSTools.Media.FFmpeg;

namespace GrabScreen
{
    class Program
    {
        static void Main(string[] args)
        {
			Program objPro = new Program();
			objPro.GrabScreen();
        }

		private void GrabScreen()
		{
			////////////////////////////////////////////////////////////////////////////////////


			using (FFmpegMediaInfo objMediaInfo = new FFmpegMediaInfo())
			{
				objMediaInfo.OpenFileOrUrl("https://iontv.akamaized.net/india/dozer/tvderana1/tracks-v1a1/index.m3u8");

				TimeSpan dTS = objMediaInfo.Duration;
				string duration = String.Format("{0}:{1:00}:{2:00}", dTS.Hours, dTS.Minutes, dTS.Seconds);

				Size ImgSize = objMediaInfo.VideoResolution;
				string resolution = String.Format("{0}x{1}", ImgSize.Width, ImgSize.Height);

				FFmpegStreamInfo sInfo = objMediaInfo.Streams.FirstOrDefault(v => v.StreamType == FFmpegStreamType.Video);

				List<VideoFrameEntry> VideoFrame = new List<VideoFrameEntry>();
				if (sInfo != null)
				{
					// Prepare random timestamps
					Random rnd = new Random();
					long dTicks = objMediaInfo.Duration.Ticks;
					TimeSpan t1 = new TimeSpan(Convert.ToInt64(dTicks * rnd.NextDouble()));
					TimeSpan t2 = new TimeSpan(Convert.ToInt64(dTicks * rnd.NextDouble()));

					// Extract images
					VideoFrame = objMediaInfo.GetFrames(objMediaInfo.Streams.IndexOf(sInfo), new List<TimeSpan>() { t1, t2 }, true, (index, count) =>
					{
						// Get the progress percentage
						double percent = Convert.ToDouble(index) / Convert.ToDouble(count) * 100.0;

						return false; // Not canceling the extraction
					}
					);
					Bitmap bImg = VideoFrame[0].Picture;

					bImg.SaveAsJpeg("MyScreenImage.jpg", 100000);
				}
			}
		}
    }
}
