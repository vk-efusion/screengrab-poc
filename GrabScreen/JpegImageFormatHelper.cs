﻿using System.Drawing.Imaging;
using System.Drawing;

namespace BjSTools.Forms
{
    public static class JpegImageFormatHelper
    {
        public static ImageCodecInfo GetJpegCoder()
        {
            ImageCodecInfo jpegCoder = null;
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == ImageFormat.Jpeg.Guid)
                {
                    jpegCoder = codec;
                    break;
                }
            }
            return jpegCoder;
        }

        public static EncoderParameters GetJpegQualityParameters(long quality)
        {
            EncoderParameters result = new EncoderParameters(1);
            result.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
            return result;
        }

        public static void SaveAsJpeg(this Bitmap img, string filename, long quality)
        {
            ImageCodecInfo encoder = GetJpegCoder();
            EncoderParameters encoderParams = GetJpegQualityParameters(quality);
            img.Save(filename, encoder, encoderParams);
        }
    }
}
